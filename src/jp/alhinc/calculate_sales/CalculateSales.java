package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// 正規表現
	private static final String BRANCH_CODE_RULE = "^[0-9]{3}$";
	private static final String COMMODITY_CODE_RULE = "^[0-9a-zA-Z]{8}$";
	private static final String SALESFILE_NAME_RULE = "^[0-9]{8}\\.rcd$";
	private static final String SALES_AMOUNT_RULE = "^[0-9]+$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String SALES_FILE_NOT_EXIST = "支店定義のファイルが存在しません";
	private static final String COMMODITY_FILE_NOT_EXIST = "商品定義のファイルが存在しません";
	private static final String SALES_FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String COMMODITY_FILE_INVALID_FORMAT = "商品定義ファイルのフォーマットが不正です";
	private static final String FILE_NOMBER_ORDER = "売り上げファイル名が連番になっていません";
	private static final String FILE_TOTAL_OVER = "合計金額が10桁を超えました";
	private static final String FILE_BRANCH_CODE_FORMAT = "の支店コードが不正です";
	private static final String FILE_COMMODITY_CODE_FORMAT = "の商品コードが不正です";
	private static final String SALESFILE_NAME_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		// コマンドライン引数が1つに設定されていなかった場合
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH_CODE_RULE, SALES_FILE_NOT_EXIST, SALES_FILE_INVALID_FORMAT)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, COMMODITY_CODE_RULE,  COMMODITY_FILE_NOT_EXIST, COMMODITY_FILE_INVALID_FORMAT)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			//	ファイルが正しいかつ売上ファイルがフォーマットにあっていれば、rcdFilesに格納
			if(files[i].isFile() && files[i].getName().matches(SALESFILE_NAME_RULE)) {
				rcdFiles.add(files[i]);
			}
		}

		//　ファイルの順番確認
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			// 比較する2つのファイル名の先頭から数字の8文字を切り出し、int型に変換します。
			int former = Integer.parseInt((files[i].getName()).substring(0,8));
			int latter = Integer.parseInt((files[i + 1].getName()).substring(0,8));

			// 次のファイルが調べているファイルより1多いか調べる
			if((latter - former) != 1) {
				System.out.println(FILE_NOMBER_ORDER);
				return;
			}
		}

		//	rcdFilesリストに入れたファイルをリストに入れて
		//	入れたリストの中身をbranchSales.Mapにput
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			ArrayList<String> salesLine = new ArrayList<String>();

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;

				// 一行ずつ読み込む
				while((line = br.readLine()) != null) {
					salesLine.add(line);
				}

				// 売上ファイルの行数が3行ではなかった場合
				if(salesLine.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + SALESFILE_NAME_FORMAT);
					return;
				}

				// 支店定義ファイルに売り上げファイルのコードが存在するか確認
				if (!branchSales.containsKey(salesLine.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_BRANCH_CODE_FORMAT);
					return;
				}

				// 商品定義ファイルに売り上げファイルのコードが存在するか確認
				if (!commoditySales.containsKey(salesLine.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_COMMODITY_CODE_FORMAT);
					return;
				}

				// 売り上げファイルの金額が数字でなかった場合
				if(!salesLine.get(2).matches(SALES_AMOUNT_RULE)) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//	branchSalesのvalueの型を定義
				// このループでリストに入れていた売上金額
				long fileSale = Long.parseLong(salesLine.get(2));
				//このループの支店コードに紐づいた呼び出した売上金額を足す
				Long branchSaleAmount = branchSales.get(salesLine.get(0)) + fileSale;
				//このループの商品コードに紐づいた呼び出した売上金額を足す
				Long commoditySaleAmount = commoditySales.get(salesLine.get(1)) + fileSale;

				// 合計金額10桁確認
				if(branchSaleAmount  >= 10000000000L || commoditySaleAmount >= 10000000000L){
					System.out.println(FILE_TOTAL_OVER);
					return;
				}

				branchSales.put(salesLine.get(0), branchSaleAmount );
				commoditySales.put(salesLine.get(1), commoditySaleAmount );

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			}  finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					} finally {

					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コード,商品コードと支店名,商品名を保持するMap
	 * @param 支店コード,商品コードと売上金額を保持するMap
	 * @param 正規表現のルール
	 * @param ファイルが存在しない場合のエラー文
	 * @param ファイルのフォーマットが違っている場合のエラー文
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales , String codeRule, String fileNotExist, String fileInvalidFormat) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//	ファイルの存在チェック
			if(!file.exists()) {
				System.out.println(fileNotExist);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items =  line.split(",");

				// ファイルフォーマット確認
				if((items.length != 2) || (!items[0].matches(codeRule))){
					//	エラーメッセージをコンソールに表示。
					System.out.println(fileInvalidFormat);
					return false;
				}

				names.put(items[0], items[1]);
				//	一度ここで、Mapの型のみ定義して中身を入れられるようにする。
				sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				} finally {

				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		File file = new File(path, fileName);
		BufferedWriter bw = null;

		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : Names.keySet()) {
				//　このループ（Names（map）のkey（支店コード,商品コード））に紐づいた呼び出した売上金額
				Long saleAmount = Sales.get(key);

				bw.write(key + "," + Names.get(key) + "," + saleAmount);
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(e);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				} finally {

				}
			}
		}

		return true;
	}

}
